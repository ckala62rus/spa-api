<?php

namespace App\Http\Controllers;

use App\Http\Resources\BooksResourse;
use App\Model\Books;
use App\Model\TodoBlock;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index()
    {
//        echo 'Vue spa example and laravel 6.0';

//        $books = [
//            0 => [
//                'id' => 1,
//                'title' => 'title',
//                'page' => '10',
//                'total' => '100',
//                'category_id' => 1,
//            ],
//            1 => [
//                'id' => 2,
//                'title' => 'Война и мир',
//                'page' => '1000',
//                'total' => '100',
//                'category_id' => 1,
//            ],
//            2 => [
//                'id' => 3,
//                'title' => 'Наука и жизнь',
//                'page' => '500',
//                'total' => '100',
//                'category_id' => 1,
//            ],
//        ];
        $books = Books::all();
//        dd($books);
        return response()->json([
            'data' => BooksResourse::collection($books),
//            'data' => $books,
            'count' => count($books),
        ]);
    }

    /*
     * Получение всех блоков (задач)
     */
    public function getBlock()
    {
        $block = TodoBlock::select('id', 'status', 'title')->get();
        return response()->json( $block );
    }

    /*
     * Перезаписывание состояния (задачи) блока при перетаскивании
     */
    public function setUpdateBlock( Request $request )
    {
        TodoBlock::updateOrCreate(
            ['id' => $request->update_block_id],
            ['status' => $request->update_block_status]
        );
        return response()->json([]);
    }

    /**
     * Получение данных по одному блоку
     */
    public function getBlockById(Request $request)
    {
        return response()->json( TodoBlock::where('id', $request->id)->get() );
    }

    /**
     * Изменение данных конкретной задачи
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editBlock(Request $request)
    {
        $data = $request->input()[0];
        $model = TodoBlock::updateOrCreate(
            [
                'id' => $data['id'],
                'status' => $data['status'],
            ],
            [
                'title' => $data['title'],
                'description' => $data['description'],
            ]
        );
        return response()->json($model);
    }
}
