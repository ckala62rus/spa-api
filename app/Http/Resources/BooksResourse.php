<?php

namespace App\Http\Resources;

use App\Model\Books;
use App\Model\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class BooksResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'category' => CategoryResourse::collection(Category::where('id', $this->category)->get()),
        ];
    }
}
