<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TodoBlock extends Model
{
    protected $table = 'todo_block';
    public $timestamps = false;

    protected $fillable = [
        'status',
        'title',
        'description',
    ];
}
