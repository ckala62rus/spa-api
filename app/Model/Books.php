<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = 'books';

    public function category()
    {
        return $this->hasOne( Category::class, 'id', 'category');
    }
}
