<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    public function book()
    {
        return $this->hasMany( Books::class, 'category', 'id');
    }
}
